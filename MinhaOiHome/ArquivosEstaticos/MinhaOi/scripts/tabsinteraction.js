$(document).ready(function(){
	tabsInteraction();
	loadContaFatura();
});
function tabsPanelsResize() {
	
	$('.panel-vertical .account-tabs').each( function() { // mantem a lista de tabs com mesma altura que o painel
		panelHeight = 0
		$(this).siblings('.accountBox, .panel').each( function() {
			panelHeight = ($(this).innerHeight() >= panelHeight) ? $(this).innerHeight() : panelHeight;
		});
		if (!$(this).find('div.tabsparent').length)
			$(this).find('ul').wrap('<div class="tabsparent"/>')
		$(this).find('div.tabsparent').height(panelHeight - 80).end();
		$(this).find('div.tabsparent ul').height(7000);
		$(this).height($(this).siblings('.accountBox, .panel').innerHeight());

		$(this).siblings('.accountBox, .panel').height(panelHeight);
		$(this).parents('.panel-vertical').height(panelHeight);

	});
	
	$('.panel-horizontal .account-tabs').each( function() { // mantem a lista de tabs com mesma largura que o painel
		if (!$(this).find('div.tabsparent').length)	$(this).find('ul').wrap('<div class="tabsparent"/>')
		$(this).find('ul').width(50000);
	});
	
	atribuiTabs();
	
}

// Controla a interacao das abas de conta - leo.carvalho
function tabsInteraction() {
	
	tabsPanelsResize();
	atribuiTabs();
	//selectPdf();/*40727 - Doc one leo.carvalho 10/10/2012*/
	
	$('.panel-horizontal .accountBox').hide(); // esconde os paineis e exibe somente o primeiro - novo layout leo.carvalho
	
	$('.panel-horizontal').each( function() {
		$(this).find('.accountBox').eq(0).show();
	});
	
	if ($('.accountBox.panel-active').length) {
		$('.accountBox').hide();
		$('.accountBox.panel-active').show();
	}

	$('.panel-horizontal').each( function() {
		updateAccountTabsScroll($(this));
	});
	
	$('.account-tabs li').on('click', function(e) { // troca o painel no clique da aba - leo.carvalho
		$(this).addClass('active').siblings().removeClass('active');

		$(this).parents('.panel-horizontal').find('.accountBox').hide();
		var linkhref = $(this).find('a').attr('href').split('#');
		var destdiv = $('#' + linkhref[1]);
		destdiv.show();
		var paramtCanal = linkhref[0].split('?');
		var contCanal = '&' + paramtCanal[1];
		if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
			destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

			// verifica se foi efetivamente clicado na aba ou se foi acionado via trigger * anderson.gulao em 07/08/2012
			if(e.which) {
		        //Actually clicked
				$('aside').html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');
		    } //Else, clicked by trigger
			$.ajax({
				url: linkhref[0],
				dataType: 'html',
				type: 'get',
				async: true,
				success: function(dados,textStatus, request) {
					$(destdiv).empty().append(innerShiv(dados,false));
					
					if (!($(destdiv).find('.account-message').length)) {
						$(destdiv).append('<div class="account-footerPanel"> </div>');
					}
					// caso a resposta do AM seja um redirect ou acesso negado, voltar para a tela de login
					var nStatus = request.status;
					if (nStatus == 0 || nStatus == 401 || (nStatus >= 300 && nStatus < 400)){
					window.location = '/';
					return;
					}
					//correcao do menu para IE    ***** leo.carvalho
					$.getScript("/ArquivosEstaticos/MinhaOi/scripts/menu.js");
					trataConteudo(destdiv);
					trataBotao(destdiv);
					ieFixes();
					tabsPanelsResize();
					//accountTabsIEFix();removido em PRD
					createTooltipIcos();
					//tabsMiolo();//Solu??o provis?ria 55772 QA - leo.carvalho
					// verifica se foi efetivamente clicado na aba ou se foi acionado via trigger * anderson.gulao em 07/08/2012
					
					//selectPdf();/*40727 - Doc one leo.carvalho 10/10/2012*/
					if(e.which) {
				        //Actually clicked
						var urlSideBar = '/portal/site/MinhaOi/SideBar?vgnextoid=' + canalCorrenteVcmId + contCanal + '&vgnextrefresh=1';
						$.ajax({
							url: urlSideBar,
							dataType: 'html',
							type: 'get',
							async: true,
							success: function(dados,textStatus, request) {
								$('aside').empty().append(innerShiv(dados,false));
							}
						});
				    } //Else, clicked by trigger
				}
			});
		}
        
		if ($(this).parents('.panel-vertical, .panel-horizontal').hasClass('panel-vertical')) {
			var alturaPai = parseInt($(this).parents('div.tabsparent').css('margin-top'), 10);
			var alturaLista = parseInt($(this).parents('ul')[0].offsetTop, 10);
			var tamanhoLista = parseInt($(this).parents('div.tabsparent').innerHeight(), 10);
			var alturaAba = parseInt(this.offsetTop, 10);
			var tamanhoAba = parseInt($(this).innerHeight(), 10);
			
			if ((alturaAba + alturaLista) > (tamanhoLista - tamanhoAba)) {
				$(this).parents('ul').css('top', '-' + ((alturaAba + alturaPai) - (tamanhoLista - tamanhoAba)) + 'px');
			} else if ((alturaAba + alturaLista) < 0) {
				$(this).parents('ul').css('top', '-' + alturaAba + 'px');
			}
			
		} else if ($(this).parents('.panel-vertical, .panel-horizontal').hasClass('panel-horizontal')) {     
			
			var esquerdaPai = parseInt($(this).parents('div.tabsparent').css('margin-left'), 10);
			var esquerdaLista = parseInt($(this).parents('ul')[0].offsetLeft, 10);
			var larguraLista = parseInt($(this).parents('div.tabsparent').innerWidth(), 10);
			var esquerdaAba = parseInt(this.offsetLeft, 10);
			var larguraAba = parseInt($(this).innerWidth(), 10);
						
			if ((esquerdaAba + esquerdaLista) > (larguraLista - larguraAba)) {
				$(this).parents('ul').css('left', '-' + ((esquerdaAba + esquerdaPai) - (larguraLista - larguraAba)) + 'px');
			} else if ((esquerdaAba + esquerdaLista) < 0) {
				$(this).parents('ul').css('left', '-' + esquerdaAba + 'px');
			}
			
		}
		
		updateAccountTabsScroll($(this).parents('.panel-horizontal'));
		return false;
				
  });
		
	//leitura direta para abas vertical e horizontal sem timeout - leo.carvalho
	$('.account-tabs li.autoLoad').trigger('click');
    
		$('.scroll').on('click', function(){
        var divtabs = $(this).parents('.account-tabs').find('div.tabsparent');
        var ultabs = divtabs.find('ul');
				var ultabs_el = divtabs.find('ul li:first').outerWidth(true);
        
				
				if ($(this).parents('.panel-vertical, .panel-horizontal').hasClass('panel-horizontal')) {
					if (ultabs.width() > divtabs.width()) {
					
						if ($.browser.msie && parseFloat($.browser.version) > 7) {
							if ($(this).hasClass('up')) {
							
									ultabs.css('left', (ultabs.position().left + ultabs_el - 0.0833129882812) + 'px');
									if (ultabs.position().left > 0){ 
										ultabs.css('left', 0);
									}
							}	else {

								var finaldaultimaaba = parseInt(ultabs.find('li:last').offset().left, 10) + parseInt(ultabs.find('li:last').width(), 10);
								var diferencaminima = ((4 + finaldaultimaaba - parseInt(ultabs.offset().left, 10)) - parseInt(divtabs.width(), 10)) * -1;

								if (finaldaultimaaba > parseInt(divtabs.offset().left, 10) + parseInt(divtabs.width(), 10)) {
									ultabs.css('left', (ultabs.position().left - ultabs_el + - 0.0833129882812) + 'px');

									if (ultabs.position().left < diferencaminima) { 
										ultabs.css('left', diferencaminima + 'px');
									}
								}
							}
						}else{
								if ($(this).hasClass('up')) {
									if (ultabs.offset().left < divtabs.offset().left) {
										ultabs.css('left', (ultabs.position().left + ultabs_el) + 'px');
										if (ultabs.position().left > 0){ 
											ultabs.css('left', 0);
										}

									}
								}	else {

									var finaldaultimaaba = parseInt(ultabs.find('li:last').offset().left, 10) + parseInt(ultabs.find('li:last').width(), 10);
									var diferencaminima = ((5 + finaldaultimaaba - parseInt(ultabs.offset().left, 10)) - parseInt(divtabs.width(), 10)) * -1;

									if (finaldaultimaaba > parseInt(divtabs.offset().left, 10) + parseInt(divtabs.width(), 10)) {
										ultabs.css('left', (ultabs.position().left - ultabs_el) + 'px');

										if (ultabs.position().left < diferencaminima) { 
											ultabs.css('left', diferencaminima + 'px');
										}

									}
								}
							
						}
					}
        }
				
        else if ($(this).parents('.panel-vertical, .panel-horizontal').hasClass('panel-vertical')) {
					if (ultabs.height() > divtabs.height()) {
						if ($(this).hasClass('up')) {
								if (ultabs.offset().top < divtabs.offset().top) {
										ultabs.css('top', (ultabs.position().top + 50) + 'px');
										if (ultabs.position().top > 0) 
												ultabs.css('top', 0);
								}
						}
						else {
								var finaldaultimaaba = parseInt(ultabs.find('li:last').offset().top, 10) + parseInt(ultabs.find('li:last').height(), 10);
								var diferencaminima = ((5 + finaldaultimaaba - parseInt(ultabs.offset().top, 10)) - parseInt(divtabs.height(), 10)) * -1;
								if (finaldaultimaaba > parseInt(divtabs.offset().top, 10) + parseInt(divtabs.height(), 10)) {
										ultabs.css('top', (ultabs.position().top - 50) + 'px');
										if (ultabs.position().top < diferencaminima) 
												ultabs.css('top', diferencaminima + 'px');
								}
						}
					}
				}
				
        updateAccountTabsScroll($(this).parents('.panel-vertical, .panel-horizontal'));
        return false;
    });
    
    $('.panel-vertical .account-tabs li').prepend('<small />'); // estilo da aba
}

// Chamada em tabsInteraction(). Controla o scroll das abas de conta, horizontal e vertical.
function updateAccountTabsScroll(objpanel) {
	var divtabs = $(objpanel).find('.account-tabs div.tabsparent');
	var ultabs = divtabs.find('ul');
	var lnks = $(objpanel).find('.scroll');
	
	
	if ($(objpanel).hasClass('panel-horizontal')) {
		if (ultabs.width() >= divtabs.width()) {
			for (var l = 0; l < lnks.length; l++) {
				var lnk = lnks[l];
				var maxHtabs = 4
				
				if ($(lnk).hasClass('up')) {
					
					if (ultabs.position().left >= 0) {
						$(lnk).removeClass('upActive').addClass('upInactive');
					} else {
						$(lnk).addClass('upActive').removeClass('upInactive');
					}
				
				} else {

					var finaldaultimaaba = parseInt(ultabs.find('li:last').offset().left, 10) + parseInt(ultabs.find('li:last').width(), 10);
					var diferencaminima = ((finaldaultimaaba - parseInt(ultabs.offset().left, 10)) - parseInt(divtabs.width(), 10)) * -1;
					
					if (ultabs.position().left <= diferencaminima) {
						$(lnk).removeClass('downActive').addClass('downInactive');
					} else {
						$(lnk).addClass('downActive').removeClass('downInactive');
					}
					
				}
				if (ultabs.find('li').length <= maxHtabs) {
					$(lnk).css('display', 'none');
				}
				
      }
    }
  }
		
  var scrolled=false;
  
	for(var l=0;l<lnks.length;l++){
		var lnk=lnks[l];
    if ($(lnk).hasClass('upActive') || $(lnk).hasClass('downActive')) {
			scrolled=true;
      break;
    }
  }
		
  if(!scrolled) lnks.hide();
}

// 8180 / 8182 / 8184 - Novo Layout
function tabsInteractionNovo(nav) {
	var linkhref;
	if(nav !=''){
		linkhref= $(nav).find('a').attr('href').split('#');
	}

	var destdiv = $('#' + linkhref[1]);
	destdiv.show();
	if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
		destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

		$.ajax({
			url: linkhref[0],
			dataType: 'html',
			type: 'post',
			async: true,
			success: function(dados,textStatus, request) {
				// Preenche o ajax da home inicio
				$(destdiv).empty().append(innerShiv(dados, false));
				carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
				carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');

				$.getScript("/ArquivosEstaticos/MinhaOi/js/minhaoi.js");
				createTooltipIcos();
			}
		});

	}

}


function loadContaFatura(){
	$('.nav-conta-fatura').each( function() {
		tabsInteractionNovo(this);
	});
}

