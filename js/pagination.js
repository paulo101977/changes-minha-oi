$(document).ready(function(){
  var articleLimit = 5;
  var currentPage = 1;
  var $currentPage = $('.current-page');
  var $cupons = $('.cupom-index');
  var totalArticles = $cupons.length;
  var totalPages = (totalArticles / articleLimit);

  $cupons.hide();
  changePage();

  $('.next').on('click', function(){
  	currentPage++;
    if (currentPage > totalPages) {
    	currentPage = 1;
    }
    changePage();
    return false;
  });
  $('.prev').on('click', function(){
  	currentPage--;
    if (currentPage < 1) {
    	currentPage = totalPages;
    }
    changePage();
    return false;
  });

  function changePage() {
  	$cupons.hide();
    $($cupons[currentPage * articleLimit - 1]).show();
    $($cupons[currentPage * articleLimit - 2]).show();
    $($cupons[currentPage * articleLimit - 3]).show();
    $($cupons[currentPage * articleLimit - 4]).show();
    $($cupons[currentPage * articleLimit - 5]).show();
  }

  $('a.back').click(function(){
		parent.history.back();
		return false;
	});

});
