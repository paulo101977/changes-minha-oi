$(document).ready(function(){

  function formatNumberComma (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }

  function formatNumber(num){
     num = num/1000;

    if(num == parseInt(num)){
      return formatNumberComma(parseInt(num));
    }

    return formatNumberComma(num.toFixed(2));
  }

  //server side values
  var initialUserMapValues = {
    'minValue': 10, //min value
    'usersInfo': [
      //first
      {
        name: 'Marianna',
        phone: '(21) 98712-0921',
        value: 10,
        avulso: 2000,
        bloqueado: true
      },
      //second
      {
        name: 'Marcela',
        phone: '(21) 92812-0992',
        value:10,
        avulso: 0,
        bloqueado: true
      },
      //third
      {
        name: 'Ludmilla',
        phone: '(21) 98712-0921',
        value: 10,
        avulso: 0,
        bloqueado: true
      },
      //fourth
      {
        name: 'Henrique',
        phone: '(21) 99912-2912',
        value: 210,
        avulso: 0,
        bloqueado: true
      },
      //fifth
      {
        name: 'Fernando',
        phone: '(21) 98712-0921',
        value: 4100,
        avulso: 2000,
        bloqueado: true
      }
    ]
  }

var currentMapValues = [];

var usersInfo = initialUserMapValues.usersInfo;
var minValue = initialUserMapValues.minValue;
var formatMinValue = formatNumber(minValue);
function addAtor() {

  var formTransferencia = $(".form-transferencia");
  var originalBoxTransf = $('.panel.box-transferir-atores');


  usersInfo.forEach(function(value, index){
    if(index != 0) {
      var box = originalBoxTransf.clone()
      box.addClass('ator-' + (index + 1))
      box.find('.panel-heading').attr('id', 'value-box-transf-' + index)
      box.find('.panel-heading__itemName').text(usersInfo[index].name);
      box.find('.panel-heading__itemNumber').text(usersInfo[index].phone);
      box.find('.first-value').text(formatNumber(usersInfo[index].value) + " GB ");
      box.find( "input[type=checkbox]").val(formatNumber(usersInfo[index].value));
      if(usersInfo[index].avulso) {
        box.find('.first-avulso-value').text("+" + formatNumber(usersInfo[index].avulso) +" GB avulso")
      }
      if(formatNumber(usersInfo[index].value) <= formatMinValue) {
        box.find('.first-value').addClass("low-qty");
        box.find('.first-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");
      }
      formTransferencia.append(box)
    }
  })
  originalBoxTransf.addClass('ator-1');
  originalBoxTransf.find('.panel-heading').attr('id', 'value-box-transf-0');
  originalBoxTransf.find('.panel-heading__itemName').text(usersInfo[0].name);
  originalBoxTransf.find('.panel-heading__itemNumber').text(usersInfo[0].phone);
  originalBoxTransf.find('.first-value').text(formatNumber(usersInfo[0].value) + " GB ");
  originalBoxTransf.find( "input[type=checkbox]").val(formatNumber(usersInfo[0].value));

  if(usersInfo[0].avulso) {
    originalBoxTransf.find('.first-avulso-value').text("+" + formatNumber(usersInfo[0].avulso) +" GB avulso")
  }
  if(formatNumber(usersInfo[0].value) <= formatMinValue) {
    originalBoxTransf.find('.first-value').addClass("low-qty");
    originalBoxTransf.find('.first-value').siblings(".panel-heading__itemQty--lower").addClass("low-qty");
  }

}

function addClassCheck() {
  $('.add-ator').change(function() {
  var $check = $(this),
  $div = $check.parent();
  if($(this).is(':checked')) {
    $div.addClass('active');
    $check.siblings().addClass('active');
    $check.siblings(".panel-body").children(".panel-heading__itemQty").children().addClass('white-color');
  } else {
      $div.removeClass('active');
      $check.siblings().removeClass('active');
      $check.siblings(".panel-body").children(".panel-heading__itemQty").children().removeClass('white-color');
    }
  })
  changeInputCheck();
}

function changeInputCheck() {
  var checkState = [];
  $("input:checkbox").change(function() {
      if($(this).is(':checked')) {
          checkState.push(this);
          if(checkState.length > 2) {
            var shiftChecked = $(checkState.shift());
            shiftChecked.prop('checked',false).siblings().removeClass('active');
            shiftChecked.parent().removeClass('active');
            shiftChecked.siblings(".panel-body").children(".panel-heading__itemQty").children().removeClass('white-color');
              }
      } else {
          var ix = checkState.indexOf(this);
          if(ix > -1) checkState.splice(ix,1);
      }
  });
 disableMinValue();
}

function disableMinValue() {
  $(":checkbox").change(function(){
    var checkMin = $(":checkbox[value='0.01']:not(:checked)");
    var inputMin = $(":checkbox[value='0.01']:checked");
    var checkValue = ($(this).val());
    if (checkValue === formatMinValue && inputMin.length ==1) {
    $(checkMin).addClass("dipnone-second");
      checkMin.parent(".panel").click(function () {
         $('.dois-minimos').removeClass('dipnone');
      })
    }
    else {
        $(checkMin).removeClass("dipnone-second");
    }
    $('.dois-minimos').addClass('dipnone');
  })
}

function countChecked() {
  var n = $( "input:checked" ).length;
  if (n <= 1) {
    $(".warn-message-info.selecionar-2-atores").removeClass("dipnone");
  } else {
    $(".warn-message-info.selecionar-2-atores").addClass("dipnone");
  }
}

function removeMsgCheck() {
  $( "input[type=checkbox]" ).on( "click", function() {
    $(".warn-message-info.selecionar-2-atores").addClass("dipnone");
  });
}

$( ".btn-transferir-saldo" ).on( "click", function() {
  countChecked();
  removeMsgCheck();
});
addAtor();
addClassCheck();
})
