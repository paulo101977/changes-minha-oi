var values =
{
  'max':15000, //15GB
  'adesao': 7500,
  'valor': 3000
}

var datasets = {
  "0":{
    phone: "(21) 99639-7564",
    tipo: 'media',
    avulso: 3000,
    dataset:[
      { tipo: 'cheio', valor: 2100 },
      { tipo: 'total', valor: 4000 }]
  },
  "1":{
    phone: "(21) 99639-7565",
    tipo: 'esgotando',
    avulso: 3000,
    dataset: [
      { tipo: 'esgotando', valor: 6500 },
      { tipo: 'total', valor: 4000 }]
  },
  "2":{
    tipo: 'erro'
  },
  "3":{
    phone: "(21) 99639-7565",
    tipo: 'cheio',
    avulso: 1000,
    dataset: [
      { tipo: 'cheio', valor: 2600 },
      { tipo: 'total', valor: 4000 }]
  },
  "4":{
    phone: "(21) 99639-7565",
    tipo: 'esgotada',
    avulso: 0,
    dataset: [
      { tipo: 'esgotada', valor: 4000 },
      { tipo: 'total', valor: 4000 }]
  }
}

//var index = 0;


function setLabel(max){
  $(".label-min").text('0 GB')
  $(".label-middle").text(formatNumber(max/2)+ ' GB')
  $(".label-max").text(formatNumber(max) + ' GB')
}

function setDataLabel(data){
  $('.current-data .data').text(formatNumber(data) + " GB");
}

function setDataLabelPos(pos){
  var comp = $('.current-data');
  var width = comp.width();
  comp.css({left: pos - width/2 - 2})
}

function setInfoButton(pos){
  var info = $('.wrapper-graphic-cingapura .info');
  info.toggle();

  // info.css({right: pos/2 - info.width()/2})

  $('[data-toggle="tooltip-info"]').tooltip(
    {
      html: true,
      title:
        'A sua internet é de <strong>' + formatNumber(values.max) + ' GB</strong>. '
        +'A franquia deste mês é proporcional à data de adesão ao plano.'
      });
}

$(document).ready(function(){
  var width = $('.container-graphic').width();


  var adesao = values.adesao;
  var max = values.max;
  var valor = values.valor;
  var valorComp = $('.valor-atual');
  var adesaoComp = $('.proporcional-adesao');
  var adesaoWidth = 0;
  var valorWidth = 0;

  setDataLabel(valor)

  //set initial label
  //setLabel(max)

  if(values.adesao){
    //adesaoWidth = width*adesao/max;
    //adesaoComp.width(adesaoWidth)
    //valorWidth = width*valor/max - adesaoWidth;
    valorWidth = width*valor/adesao;
    valorComp.width(valorWidth)

    setLabel(values.adesao)

    setInfoButton(adesaoWidth);
    setDataLabelPos(adesaoWidth + valorWidth)
  }
  else{
    $('i.info').hide();
    setLabel(max)
    valorWidth = width*valor/max;
    valorComp.width(valorWidth)
    setDataLabelPos(valorWidth)
  }

  $('.ver-detalhes-por-pessoa__').click(function(){
    $('#detalhes-por-pessoa__').collapse('toggle')
  })

  //listeners
  $('#detalhes-por-pessoa__').on('hide.bs.collapse', function () {
    $('.ver-detalhes-por-pessoa__').text('Ver consumo detalhado por pessoa')
  })

  $('#detalhes-por-pessoa__').on('show.bs.collapse', function () {
    $('.ver-detalhes-por-pessoa__').text('Ocultar consumo detalhado por pessoa')

    if(typeof animateArc  !== 'undefined'){
      //console.log('drawChart' , drawChart)
      $.each( datasets, function( key, item ){
        if(item.tipo != 'erro'){
          animateArc(key, item.dataset, item.avulso)
        }
      })
    }
  })

  //draw pie chart and info
  $.each( datasets, function( key, item ) {
    //console.log(dataset)
    //chartContainer.append(createComponent(index))
    //console.log('index' , index)
    //console.log('item' , item)
    var index = parseInt(key);

    console.log(index)

    if(item.tipo != 'erro'){
      //cria components
      createComponent(
        index, //mude para key 0...length se necessário
        item.phone ,
        datasets,
        item.dataset ,
        item.avulso,
        item.tipo
      )
      //desenha o chart
      drawChart("#chart" + index , item.dataset , item.avulso , index)
    } else {
      drawErro(index);
    }

    //index++;
  });
})

//var chartContainer = $(".charts-dados-navegados");

function formatNumberComma (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function formatNumber(num){
    num = num/1000;

    if(num == parseInt(num)){
      return formatNumberComma(parseInt(num));
    }

    return formatNumberComma(num.toFixed(1));
}

function createComponent(index, phone, datasets , dataset, avulso , tipo){

  var chart = d3.selectAll('.charts-dados-navegados')
    .append('div')
    .attr('class', function(){
      if(index == Object.keys(datasets).length - 1
         && ((index + 1) % 2 == 1))
      {
            return 'col-xs-12 chart-item'
      }

      return 'col-xs-6 chart-item'
    })

  //phone
  chart.append('div')
    .attr('class','phone')
    .text(phone);

  //donut
  chart.append('div')
    .attr('class','widget')
    .append('div')
    .attr({
      id: 'chart' + index,
      class: 'chart-container'
    })

  //valores
  var info = chart.append('div')
      .attr('class','chart-info')

  info.append('div')
    .attr('class','valor')
    .text(formatNumber(dataset[0].valor) + " GB")

  info.append('div')
    .attr('class','total')
    .text("de " + formatNumber(dataset[1].valor) + " GB")

  if(avulso){
    var avulsoEl = info.append('div')
      .attr('class','avulso')
      .attr('id' , 'tooltip-activator-avulso' + index)
      .attr('title', 'Pacotes avulsos comprados')
      .attr('data-toggle', 'tooltip-activator-avulso' + index)
    avulsoEl.text("+" + formatNumber(dataset[1].valor) + " GB*")

    /*var tooltip = info.append('div')
        .attr('class','tooltip-info teste-opacity')
        .attr('id', 'tooltip-info' + index)
    tooltip.text('Pacotes avulsos comprados')
    tooltip.append('div')
      .attr('class','tooltip-arrow-shadow')
    tooltip.append('div')
      .attr('class','tooltip-arrow')*/

      /*$('.avulso').hover(function(){
        $(this).next('#tooltip-info' + index).toggleClass('teste-opacity');
      });*/
      $('[data-toggle="tooltip-activator-avulso' + index + '"]').tooltip();
  }

  if(tipo === "esgotada" || tipo === "esgotando" || tipo === "media"){
      var message =
          chart.append('div')
                      .attr('class','msg-info')

      //<i class="material-icons">warning</i>
      message.append('i')
        .attr('class','material-icons icon-warning')
        .text('warning')

      var messageContainer = message.append('span')
        .attr('class','msg-info-container')

      var msgTxt = messageContainer.append('span')
        .attr('class', 'msg-txt')

      if(tipo === "esgotada"){
        msgTxt.text('Saldo esgotado.');
      }
      if(tipo === "esgotando") {
        msgTxt.text('Saldo esgotando.');
      }
      if(tipo === "media") {
        msgTxt.text('Consumo acima da média.');
        messageContainer.append('br')
        messageContainer.append('a')
          .attr('class','link-add testemedia')
          .attr('href','/adicionar')
          .text('Mais internet')
      }
      if(tipo === "esgotada" || tipo === "esgotando"){
        messageContainer.append('a')
          .attr('class','link-add')
          .attr('href','/adicionar')
          .text('Mais internet')
      }
  }
}

function drawErro(index){
  var chart = d3.selectAll('.charts-dados-navegados')
    .append('div')
    .attr('class', function(){
      if(index == Object.keys(datasets).length - 1
         && ((index + 1) % 2 == 1))
      {
            return 'col-xs-12 chart-item chart-item-erro'
      }

      return 'col-xs-6 chart-item chart-item-erro'
    })

  chart.append('p')
      .text(
        'Ocorreu um erro ao carregar as suas informações.'
        + ' Em alguns instantes, tente novamente.'
      )

  chart.append('a')
    .attr('class', 'btn btn-default btn-try-again btn-block')
    .text('Tentar novamente');

  //phone
  //chart.append('div')
    //.attr('class','phone')
    //.text(phone);
}




function drawChart(chart , dataset, avulso , index){
    //console.log('drawChart')

    var pie=d3.layout.pie()
      .value(function(d){return d.valor})
      .sort(null)
      .padAngle(.03);

    var w=130,h=130;

    var outerRadius=w/2;
    var innerRadius=53;

    //var color = d3.scale.category10();
    var green = "#19c40b";
    var gray = "#f5f5f5";
    var red = "#f8562c";

    var maxAngle = 270;
    var radiusMax = maxAngle * (Math.PI/180);

    var total = (avulso) ? (dataset[1].valor + avulso) : dataset[1].valor;

    //padding to linear-gradient start angle
    var angleToPadding = (dataset[0].valor)/total*maxAngle;
    var range = d3.scale.linear().domain([0, maxAngle]).range([45, 15]);
    //if this is more than 90 deg
    //console.log('angleToPadding' , range(angleToPadding))
    //angleToPadding = (angleToPadding > maxAngle/2) ? maxAngle : angleToPadding;

    var tweenValue = ((dataset[0].valor)/total*radiusMax);

    var arc=d3.svg.arc()
      .outerRadius(outerRadius)
      .innerRadius(innerRadius)
      .cornerRadius(50)
      .startAngle(0)

    var svg=d3.select("#chart" + index)
      .append("svg")
      .attr({
          id: 'svg' + index,
          width:w,
          height:h,
          class:'shadow'
      })
      .append('g')
      .attr({
          transform:'translate('+w/2+','+h/2+')'
      });

    var defs = svg.append("svg:defs")

    var red_gradient = defs.append("svg:linearGradient")
      .attr("id", "gradient" + index)
      .attr("x1", "0%")
      .attr("y1", "-10%")
      .attr("x2", "0%")
      .attr("y2", "100%")
      .attr('gradientTransform','rotate(' + range(angleToPadding) + ')')
      .attr("spreadMethod", "pad");

  red_gradient.append("svg:stop")
      .attr("offset", "0%")
      .attr("stop-color", "#ff6d00")
      .attr("stop-opacity", 1);

  red_gradient.append("svg:stop")
      .attr("offset", "100%")
      .attr("stop-color", "#dc2382")
      .attr("stop-opacity", 1);


      var background =svg
          .append('path')
          .datum(radiusMax)
          .attr({
              'class': 'background',
              'fill':gray
          })
          .attr('d', function(d) { return arc.endAngle(d)(); })

      var foreground=svg
        .append('path')
        .datum(0)
        .attr({
            'class': 'foreground',
            'fill':"url(#gradient" + index + ")"
        })
        .attr('d', function(d) { return arc.endAngle(d)(); })
        .each(function(d) { this._current = d; });

    //inverte os paths
    /*svg
      .selectAll("path")
      .sort(function (a, b) {
        //a is over b (background svg)
        //first path is over 'total'
        return 1;
     });*/
   }

function animateArc(index, dataset, avulso){


     var maxAngle = 270;
     var radiusMax = maxAngle * (Math.PI/180);
     var total = (avulso) ? (dataset[1].valor + avulso) : dataset[1].valor;
     var tweenValue = ((dataset[0].valor)/total*radiusMax);
     var w=130,h=130;
     var outerRadius=w/2;
     var innerRadius=53;

     var arc=d3.svg.arc()
       .outerRadius(outerRadius)
       .innerRadius(innerRadius)
       .cornerRadius(50)
       .startAngle(0)

       //console.log('#svg' , '#svg' + index)


    d3.select('#svg' + index)
         .select('path.foreground')
         .datum(0)//reset
         .attr('d', function(d) { return arc.endAngle(0)(); })

     d3.select('#svg' + index)
       .select('path.foreground')
       .datum(tweenValue)
       .transition()
       .delay(500)
       .duration(1000)
       .attrTween("d", function(d){
         var interpolate = d3.interpolate(this._current, d);
         this._current = interpolate(0);

         return function(t){
             return arc.endAngle(interpolate(t))();
         }
       });
   }
